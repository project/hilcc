<?php
/**
 * Creates a text file used upon Module install that creates the necessary taxonomy terms and hierarchy
 */
function hilcc_generate_taxonomy_importfile($outfile) {
	$i=0;
	#$tid=db_next_id("{term_data}_tid");
	$tid=1; # Start import from Drupal term id (tid)=0
	
	#echo "<pre>";
	$q = "select * from mty_general_hilcc where id>1 AND campo2<>'' AND campo3<>'' order by campo2";
	#db_set_active('generic_db');
	#$result=db_query($q);
	mysql_connect("localhost","","");
	mysql_select_db("generic_db");
	$result=mysql_query($q) or die("Error: ". mysql_error() );
	while($d=mysql_fetch_assoc($result)) {
		foreach($d as $n =>$v) {
			$d[$n]=trim($v);
		}
		$cat=$d["campo4"] ."|". $d["campo5"] ."|". $d["campo6"] ."|". $d["campo7"] ."|". $d["campo8"];
		$cat=preg_replace('/\|+$/', '', $cat);
		$hilcc_id=trim($d["campo1"]);
		
		#Add range reference
		$r[]=sprintf("INSERT INTO {hilcc_ranges} (hilcc_id, start, end) VALUES ('%s', '%s', '%s')", 
			$d["campo1"], $d["campo2"], $d["campo3"] );
		
		#echo "$cat\n";
		#ex: Bus & Eco|Economics|Agricultural Economies
		if (isset($cat_to_hilcc[$cat]))
			continue;
			
		#Create the tree
		$tmp=explode("|",$cat);
		$last_cat_part=$tmp[sizeof($tmp)-1];
		#unset($tmp[sizeof($tmp)-1]);
		$current_cat="";
		$prev_cat="";
		#for($level=0;$level<sizeof($tmp);$level++) {
		$level=0;
		
		echo "---------------\n";
		
		foreach($tmp as $cat_part) {
			$level++;
			echo str_repeat("*",$level) . " $cat_part";
			if (isset($part_tid[$level][$cat_part])) {
				$prev_cat=$current_cat;
				$prev_tid=$part_tid[$level][$cat_part];
				echo " (existe, tid=$prev_tid)\n";
				continue;
			}
			$current_cat .= ($current_cat!="" ? "|":"") . $cat_part;
			#$parent_tid=$cat_to_tid[$prev_cat] + 0;
			if (empty($cat_to_tid[$current_cat])) {
				#Add this
				$r[]=sprintf("INSERT INTO {term_data} (tid, name, vid) VALUES (!start_tid + %d, '%s', !vid)", $tid, quote($cat_part) );
				$po[$cat_part]=$cat_part;
				if ($prev_tid!=0 && $level>1) 
					$r[]=sprintf("INSERT IGNORE INTO {term_hierarchy} (tid, parent) VALUES (!start_tid + %d, !start_tid + %d)", $tid, $prev_tid);
				else {
					$prev_tid=0;
					$r[]=sprintf("INSERT IGNORE INTO {term_hierarchy} (tid, parent) VALUES (!start_tid + %d, 0)", $tid);
				}
				
			}
			echo " (NUEVO, tid=$tid, parent=$prev_tid)\n";
			
			if ($cat_part==$last_cat_part) {
				#Add hilcc
				$r[]=sprintf("INSERT IGNORE INTO {hilcc_terms} (tid, hilcc_id, cat) VALUES (!start_tid + %d, '%s', '%s')", $tid, $hilcc_id, quote($cat) );
			} else {
				$r[]=sprintf("INSERT IGNORE INTO {hilcc_terms} (tid, hilcc_id, cat) VALUES (!start_tid + %d, '%s', '%s')", $tid, '', quote($cat) );
			}
			
			$prev_cat=$current_cat;
			$prev_tid=$tid;
			$part_tid[$level][$cat_part]=$tid;
			$tid++;
			#print_r($r);
			#unset($r);
		}
		/*
		#Add this
		$tid++;
		$tmp=explode("|",$cat);
		$term_name=$tmp[sizeof($tmp)-1];
		$cat_to_tid[$cat]=$tid;
		
		#Add hierarchy, if it applies
		$tmp=explode("|",$cat);
		#Eliminate last term
		unset($tmp[sizeof($tmp)-1]);
		$parent_tid=0;
		if (sizeof($tmp)>0) {
			$cat_parent=implode("|", $tmp); #ex: Bus & Eco|Economics
			$parent_tid= $cat_to_tid[$cat_parent] + 0;
			echo "  Looking for parent $cat_parent... tid = $parent_tid\n";
		}
		
		#Insert into database
		$r[$i++]=sprintf("INSERT INTO {term_data} (tid, name, vid) VALUES (%d, '%s', %d)", $tid, str_replace("'","\\'", $term_name), $vid);
		$r[$i++]=sprintf("INSERT IGNORE INTO {term_hierarchy} (tid, parent) VALUES (%d, %d)", $tid, $parent_tid);
		*/
	}
	
	/*
	 * Print out queries with placeholders
	 * Lines will be something like this:
	 * INSERT INTO {term_data} (tid, name, vid) VALUES (!start_tid + 3, 'Agricultural Economics', !vid)
	 * with !tid and !vid being placeholders that will be replaced with actual values when 
	 * Drupal installs the module.
	 */
	$ok=file_put_contents( $outfile, implode("\n", $r) );
	
	
	/*
	 * Print out pot file, for future translation
	 */
	ksort($po);
	$out[]="# \$Id\$\n#\n# LANGUAGE translation of HILCC\n# Copyright YEAR NAME <EMAIL@ADDRESS>\n#, fuzzy\nmsgid \"\"\nmsgstr \"\"\n";
	foreach($po as $term) {
  	$out[]="msgid \"$term\"";
  	$out[]="msgstr \"\"\n";
	}
	$ok=file_put_contents( "pot_source.pot", implode("\n", $out) );
	
	#print_r($r);
	#db_set_active('default');
}
function quote($str) {
	return str_replace("'", "\\'", $str);
}
hilcc_generate_taxonomy_importfile("install.dat");
?>